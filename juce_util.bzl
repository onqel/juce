load('//:subdir_glob.bzl', 'subdir_glob')

def add_juce_module(module_name, deps = [], platform_deps = []):

    native.cxx_library(
        name = module_name,
        header_namespace = '',

        exported_headers = subdir_glob([('modules/' + module_name, '**/*.h')], prefix = module_name),

        platform_srcs = [
            ('macos.*',   ['modules/' + module_name + '/' + module_name + '.mm']),
            ('linux.*',   ['modules/' + module_name + '/' + module_name + '.cpp']),
            ('windows.*', ['modules/' + module_name + '/' + module_name + '.cpp'])
        ],

        exported_preprocessor_flags = ['-DJUCE_APP_CONFIG_HEADER=\"AppConfig.h\"' ] if module_name == 'juce_core' else [],
        
        platform_preprocessor_flags = [
            ('linux.*', ['-DLINUX=1', '-DJUCE_LINUX=1']),
        ],

        compiler_flags = [
            '-fPIC'
        ],

        linker_flags = [],

        exported_deps = ['juck//:juce-config'] if module_name == 'juce_core' else [],
        deps = ['//:' + d for d in deps],
        platform_deps = platform_deps,
        
        link_style = 'STATIC',
        preferred_linkage = 'STATIC',

        visibility = [ 'PUBLIC' ],
    )
