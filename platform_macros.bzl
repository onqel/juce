# Utility for creating cross platform libraries that should be 
# compiled using the apple_library rule on macOS.
# Written by John Adrian Johansen at TSE Audio 2019

def cxx_or_apple_library(name,
                         srcs = [],
                         platform_srcs = [],
                         headers = [],
                         exported_headers = [],
                         exported_platform_headers = [],
                         header_path_prefix = None,
                         header_namespace = None,
                         frameworks = [],
                         preprocessor_flags = [],
                         lang_preprocessor_flags = {},
                         platform_preprocessor_flags = [],
                         lang_platform_preprocessor_flags = {},
                         exported_preprocessor_flags = [],
                         exported_platform_preprocessor_flags = [],
                         exported_lang_preprocessor_flags = {},
                         compiler_flags = [],
                         platform_compiler_flags = [],
                         linker_extra_outputs = [],
                         linker_flags = [],
                         exported_linker_flags = [],
                         exported_platform_linker_flags = [],
                         preferred_linkage = 'any',
                         force_static = False,
                         link_style = 'static',
                         link_whole = False,
                         reexport_all_header_dependencies = True,
                         deps = [],
                         exported_deps = [],
                         tests = [],
                         extra_xcode_sources = [],
                         extra_xcode_files = [],                         
                         visibility = ['PUBLIC'],
                         labels = [],
                         licenses = []):
    # Check if we are running macOS                     
    is_macos = native.host_info().os.is_macos    
    
    # Initialize the common arguments for cxx_library and apple_library:
    args = {
      'name': name,
      'srcs': srcs,
      'platform_srcs': platform_srcs,
      'headers': headers,
      'exported_headers': exported_headers,
      'header_namespace': name if not header_namespace else header_namespace,
      'preprocessor_flags': preprocessor_flags,
      'compiler_flags': compiler_flags,
      'platform_compiler_flags': platform_compiler_flags,
      'linker_extra_outputs': linker_extra_outputs,
      'linker_flags': linker_flags,
      'exported_preprocessor_flags': exported_preprocessor_flags,
      'exported_linker_flags': exported_linker_flags,
      'exported_platform_linker_flags': exported_platform_linker_flags,
      'preferred_linkage': preferred_linkage,
      'link_style': link_style,
      'link_whole': link_whole,
      'reexport_all_header_dependencies': reexport_all_header_dependencies,
      'deps': deps,
      'exported_deps': exported_deps,
      'tests': tests,
      'visibility': visibility,
      'labels': labels,
      'licenses': licenses      
    }

    # Initialize apple_library arguments
    if is_macos:
      args['header_path_prefix'] = name if not header_path_prefix else header_path_prefix
      args['frameworks'] = frameworks
      args['extra_xcode_sources'] = extra_xcode_sources
      args['extra_xcode_files'] = extra_xcode_files
    
    # Initialize cxx_library arguments
    else:
      args['exported_platform_headers'] = exported_platform_headers
      args['lang_preprocessor_flags'] = lang_preprocessor_flags
      args['platform_preprocessor_flags'] = platform_preprocessor_flags
      args['exported_platform_preprocessor_flags'] = exported_platform_preprocessor_flags
      args['lang_platform_preprocessor_flags'] = lang_platform_preprocessor_flags
      args['exported_lang_preprocessor_flags'] = exported_lang_preprocessor_flags
      # args['exported_post_platform_linker_flags'] = exported_post_platform_linker_flags
      args['force_static'] = force_static

    # Generate platform dependent library rule
    library = native.apple_library if is_macos else native.cxx_library
    library(**args)
