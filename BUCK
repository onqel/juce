load('//:subdir_glob.bzl', 'subdir_glob')
load('//:juce_util.bzl', 'add_juce_module')

add_juce_module(
  module_name = 'juce_core',
  platform_deps = [
    ('^linux.*', [
      '//deps/linux:curl',
      '//deps/linux:dl',
      '//deps/linux:pthread'])
  ]
)

add_juce_module(
  module_name = 'juce_events',
  deps = ['juce_core'],
  platform_deps = [
    ('^linux.*', [
      '//deps/linux:webkit2gtk',
    ]),
  ],  
)

add_juce_module(
  module_name = 'juce_data_structures',
  deps = ['juce_events'],
)

add_juce_module(
  module_name = 'juce_cryptography',
  deps = ['juce_data_structures'],
)

add_juce_module(
  module_name = 'juce_graphics',
  deps = ['juce_events'],
  platform_deps = [
    ('^linux.*', [ '//deps/linux:freetype2' ]),
    ('^macos.*', []),
  ]
)

add_juce_module(
  module_name = 'juce_gui_basics',
  deps = ['juce_graphics', 'juce_data_structures'],
  platform_deps = [
    ('^linux.*', [
      '//deps/linux:X11',
      '//deps/linux:xext',
    ]),
  ],
)

add_juce_module(
  module_name = 'juce_gui_extra',
  deps = ['juce_gui_basics'],
  platform_deps = [
    ('^linux.*', [
      '//deps/linux:gtk-3.0',
      '//deps/linux:webkit2gtk',
    ]),
  ],
)

add_juce_module(
  module_name = 'juce_opengl',
  deps = ['juce_gui_extra'],
  platform_deps = [
    ('linux.*', [
      '//deps/linux:gl',
    ]),
  ],
)

add_juce_module(
  module_name = 'juce_audio_basics',
  deps = ['juce_core'],
)

add_juce_module(
  module_name = 'juce_audio_formats',
  deps = ['juce_audio_basics'],
)

add_juce_module(
  module_name = 'juce_audio_devices',
  deps = ['juce_audio_basics'],
  platform_deps = [
    ('linux.*', ['//deps/linux:alsa']),
  ],
)

add_juce_module(
  module_name = 'juce_audio_processors',
  deps = ['juce_audio_basics', 'juce_gui_extra'],
)

add_juce_module(
  module_name = 'juce_audio_utils',
  deps = [
    'juce_audio_processors',
    'juce_audio_devices',
    'juce_audio_formats',
    'juce_gui_basics'
  ],
)

cxx_library(
  name = 'juce_audio_plugin_client',
  header_namespace = '',
  exported_headers = subdir_glob([
    ('modules/juce_audio_plugin_client', '**/*.h'),
  ], prefix = 'juce_audio_plugin_client'),
  srcs = ['modules/juce_audio_plugin_client/juce_audio_plugin_client_utils.cpp'],
  exported_deps = [
    '//:juce_audio_processors'
  ],
  compiler_flags = ['-fPIC'],
  visibility = ['PUBLIC'],
)

cxx_library(
  name = 'juce_VST2',
  header_namespace = '',
  headers = [
    'modules/juce_audio_plugin_client/juce_audio_plugin_client.h'
  ],

  platform_srcs = [
      ('macosx.*',  ['modules/juce_audio_plugin_client/juce_audio_plugin_client_VST_utils.mm']),
      ('linux.*',   ['modules/juce_audio_plugin_client/juce_audio_plugin_client_VST2.cpp']),
      ('windows.*', ['modules/juce_audio_plugin_client/juce_audio_plugin_client_VST2.cpp'])
  ],

  exported_deps = [
    '//:juce_audio_plugin_client',
    '//:juce_gui_extra',
  ],

  compiler_flags = [
    '-fPIC',
    '-fvisibility=hidden',
    '-I/home/john/Development/buck/juck-plugin/SDKs/VST3_SDK',
  ],

  linker_flags = [
    '-Wl,--no-undefined'
  ],
  
  link_whole = True,
  preferred_linkage = 'STATIC',
  link_style = 'STATIC',  

  visibility = ['PUBLIC']
)
